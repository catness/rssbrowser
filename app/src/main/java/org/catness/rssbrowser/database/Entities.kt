package org.catness.rssbrowser.database

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "articles", indices = arrayOf(
        Index(value = ["date"], name = "date"),
        Index(value = ["link"], name = "link", unique = true)
    )
)
data class Article(
    var title: String = "",
    var description: String = "",
    var link: String = "",
    var creator: String = "",
    var date: Long = 0L,
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0L
)
