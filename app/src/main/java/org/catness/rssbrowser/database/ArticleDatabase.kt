package org.catness.rssbrowser.database

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Article::class], version = 1, exportSchema = false)
/*

abstract class ArticleDatabase : RoomDatabase() {
    abstract val articleDatabaseDao: ArticleDatabaseDao
}

private lateinit var INSTANCE: ArticleDatabase

fun getDatabase(context: Context): ArticleDatabase {
    synchronized(ArticleDatabase::class.java) {
        if (!::INSTANCE.isInitialized) {
            Log.i("getDatabase", "Database initialized")
            INSTANCE = Room.databaseBuilder(context.applicationContext,
                    ArticleDatabase::class.java,
                    "articles").build()
        }
    }
    return INSTANCE
}
*/
 abstract class ArticleDatabase : RoomDatabase() {

    abstract val articleDatabaseDao: ArticleDatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: ArticleDatabase? = null

        fun getDatabase(context: Context): ArticleDatabase {

            synchronized(this) {
                var instance = INSTANCE

                // If instance is `null` make a new database instance.
                if (instance == null) {
                    instance = Room.databaseBuilder(
                            context.applicationContext,
                            ArticleDatabase::class.java,
                            "articles_database"
                    )
                            .fallbackToDestructiveMigration()
                            .build()
                    INSTANCE = instance
                }

                return instance
            }
        }
    }
}

