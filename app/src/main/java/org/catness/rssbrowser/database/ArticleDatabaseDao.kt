package org.catness.rssbrowser.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ArticleDatabaseDao {
    @Query("select * from articles order by date desc")
    fun getArticles(): LiveData<List<Article>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll( articles: List<Article>)

    @Query("delete from articles where date < :key")
    fun deleteOld(key: Long)
}
