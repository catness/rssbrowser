package org.catness.rssbrowser.overview

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_article.view.*
import org.catness.rssbrowser.databinding.ListItemArticleBinding
import org.catness.rssbrowser.network.SlashdotArticle


class OverviewAdapter (private val onClickListener: OnClickListener): ListAdapter<ModelArticle, OverviewAdapter.ViewHolder>(DiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.article_link.setOnClickListener {
            onClickListener.onClick(item)
        }
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ListItemArticleBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ModelArticle) {
            binding.article = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemArticleBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    class OnClickListener(val clickListener: (article: ModelArticle) -> Unit) {
        fun onClick(article: ModelArticle) = clickListener(article)
    }
}


class DiffCallback : DiffUtil.ItemCallback<ModelArticle>() {

    override fun areItemsTheSame(oldItem: ModelArticle, newItem: ModelArticle): Boolean {
        return oldItem.id == newItem.id
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: ModelArticle, newItem: ModelArticle): Boolean {
        return oldItem == newItem
    }

}
