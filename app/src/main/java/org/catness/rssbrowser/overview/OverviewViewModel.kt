package org.catness.rssbrowser.overview

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.catness.rssbrowser.database.ArticleDatabase.Companion.getDatabase
import org.catness.rssbrowser.network.SlashdotApi
import org.catness.rssbrowser.network.SlashdotArticle
import org.catness.rssbrowser.repository.ArticleRepository
import retrofit2.await
import java.io.IOException

class OverviewViewModel(application: Application) : AndroidViewModel(application) {
    private val LOG_TAG: String = this.javaClass.simpleName

    private val articlesRepository = ArticleRepository(getDatabase(application))
    val articlesList = articlesRepository.articles

    /*
    private val _articles = MutableLiveData<List<SlashdotArticle>>()
    // The external LiveData interface to the property is immutable, so only this class can modify
    val articles: LiveData<List<SlashdotArticle>>
        get() = _articles

*/

    // The internal MutableLiveData String that stores the most recent response
    private val _response = MutableLiveData<String>()

    // The external immutable LiveData for the response String
    val response: LiveData<String>
        get() = _response

    // Create a Coroutine scope using a job to be able to cancel when needed
    private var viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

      /**
     * Event triggered for network error. This is private to avoid exposing a
     * way to set this value to observers.
     */
    private var _eventNetworkError = MutableLiveData<Boolean>(false)

    /**
     * Event triggered for network error. Views should use this to get access
     * to the data.
     */
    val eventNetworkError: LiveData<Boolean>
        get() = _eventNetworkError

    /**
     * Flag to display the error message. This is private to avoid exposing a
     * way to set this value to observers.
     */
    private var _isNetworkErrorShown = MutableLiveData<Boolean>(false)

    /**
     * Flag to display the error message. Views should use this to get access
     * to the data.
     */
    val isNetworkErrorShown: LiveData<Boolean>
        get() = _isNetworkErrorShown


    init {
        getData()
    }

    /*
    private fun getData() {
        coroutineScope.launch {

            var getArticles = SlashdotApi.retrofitService.getArticles()
            try {
                // Await the completion of our Retrofit request
                var res = getArticles.await()
                var str: String = ""
                res.itemList!!.forEachIndexed { index, it ->
                    str += "$index : ${it.title} ${it.date} ${it.creator} ${it.link}\n"
                }
                Log.i(LOG_TAG, str)
                _articles.value = res.itemList
                _response.value = "Got items: " + res.itemList!!.size.toString()
                Log.i(LOG_TAG, _response.value.toString())
            } catch (e: Exception) {
                _response.value = "Failure: ${e.message}"
                Log.i(LOG_TAG, "Failure: ${e.message}")
            }
        }
    }

     */
  private fun getData() {
        viewModelScope.launch {
            try {
                articlesRepository.refreshArticles()
                _eventNetworkError.value = false
                _isNetworkErrorShown.value = false

            } catch (networkError: IOException) {
                // Show a Toast error message and hide the progress bar.
                if(articlesList.value.isNullOrEmpty()) {
                    _eventNetworkError.value = true
                    Log.i(LOG_TAG,"network error")
                }
            }
        }
    }
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

     /**
     * Resets the network error flag.
     */
    fun onNetworkErrorShown() {
        _isNetworkErrorShown.value = true
    }



    /**
     * Factory for constructing DevByteViewModel with parameter
     */
    class Factory(val app: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(OverviewViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return OverviewViewModel(app) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }

}