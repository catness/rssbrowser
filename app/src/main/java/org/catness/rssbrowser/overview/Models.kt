package org.catness.rssbrowser.overview

import org.catness.rssbrowser.database.Article
import java.time.Instant
import  java.time.format.DateTimeFormatter
import  java.time.LocalDate
import  java.time.ZoneId

// article class used to display articles

data class ModelArticle(
    var title: String = "",
    var description: String = "",
    var link: String = "",
    var creator: String = "",
    var date: String = "",
    var id: Long = 0L
)

/**
 * Map Database entities to domain entities
 */
fun List<Article>.asDomainModel(): List<ModelArticle> {
    return map {
        ModelArticle(
            id = it.id,
            link = it.link,
            title = it.title,
            description = it.description,
            //date = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").format(java.time.Instant.ofEpochSecond(it.date)),
            date = Instant.ofEpochSecond(it.date).atZone(ZoneId.systemDefault()).toLocalDateTime()
                //.toString(),
                .format(DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm")),
            creator = it.creator
        )
    }
}



