package org.catness.rssbrowser.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.catness.rssbrowser.database.ArticleDatabase
import org.catness.rssbrowser.network.SlashdotApi
import org.catness.rssbrowser.network.asDatabaseModel
import org.catness.rssbrowser.overview.ModelArticle
import org.catness.rssbrowser.overview.asDomainModel
import retrofit2.await

class ArticleRepository(private val database: ArticleDatabase) {
    private val LOG_TAG: String = this.javaClass.simpleName

    val articles: LiveData<List<ModelArticle>> = Transformations.map(database.articleDatabaseDao.getArticles()) {
        it.asDomainModel()
    }

    /**
     * Refresh the videos stored in the offline cache.
     *
     * This function uses the IO dispatcher to ensure the database insert database operation
     * happens on the IO dispatcher. By switching to the IO dispatcher using `withContext` this
     * function is now safe to call from any thread including the Main thread.
     *
     */
    suspend fun refreshArticles() {
        withContext(Dispatchers.IO) {
            val articles = SlashdotApi.retrofitService.getArticles().await()
                var str: String = ""
                articles.itemList!!.forEachIndexed { index, it ->
                    str += "$index : ${it.title} ${it.date} ${it.link}\n"
                }
                Log.i(LOG_TAG, str)
                //database.articleDatabaseDao.deleteAll()
                database.articleDatabaseDao.insertAll(articles.asDatabaseModel())
                var currentTime = System.currentTimeMillis()/1000L;
                // var oldTime = currentTime - 86400L; // 24 hours
                var oldTime = currentTime - 2592000L; // 30 days
                database.articleDatabaseDao.deleteOld(oldTime)
        }
    }

}