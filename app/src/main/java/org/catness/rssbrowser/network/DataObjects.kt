package org.catness.rssbrowser.network

import android.text.Html
import android.text.Html.FROM_HTML_MODE_LEGACY
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml
import com.tickaroo.tikxml.converter.htmlescape.HtmlEscapeStringConverter
import org.catness.rssbrowser.database.Article
import  java.time.format.DateTimeFormatter
import  java.time.LocalDate
import java.time.LocalDateTime
import  java.time.ZoneId


@Xml(name = "rdf")
data class SlashdotResponseWrapper(
    @Element(name = "item") val itemList: List<SlashdotArticle>?
)

@Xml(name = "item", writeNamespaces = arrayOf("dc=purl.org/dc/elements/1.1","feedburner=rssnamespace.org/feedburner/ext/1.0"))
data class SlashdotArticle(
    @PropertyElement(
        name = "title",
        converter = HtmlEscapeStringConverter::class
    ) val titleEmcoded: String?,
    @PropertyElement(name = "link") val linkrss: String?,
    @PropertyElement(
        name = "description",
        converter = HtmlEscapeStringConverter::class
    ) val descriptionFull: String?,
    @PropertyElement(name = "dc:date") val date: String?,
    @PropertyElement(name = "dc:creator") val creator: String?,
    @PropertyElement(name = "feedburner:origLink") val linkFull: String?
) {
    val description
        get() = Html.fromHtml(descriptionFull?.substringBefore("<p>"), FROM_HTML_MODE_LEGACY)
    val title
        get() = Html.fromHtml(titleEmcoded,FROM_HTML_MODE_LEGACY)
    val link
        get() = linkFull?.substringBefore("?")

}

fun SlashdotResponseWrapper.asDatabaseModel(): List<Article> {
    return itemList!!.map {
        Article(
                title = it.title.toString(),
                description = it.description.toString(),
                link = it.link.toString(),
                creator = it.creator.toString(),
                date =  LocalDateTime.parse(it.date.toString(), DateTimeFormatter.ISO_OFFSET_DATE_TIME).atZone(ZoneId.systemDefault()).toEpochSecond()
        )
    }
}


/* convert string from datetime to Unix timestamp

import  java.time.format.DateTimeFormatter
import  java.time.LocalDate
import  java.time.ZoneId
val l = LocalDate.parse(str, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
val unix = l.atStartOfDay(ZoneId.systemDefault()).toInstant().epochSecond

 */