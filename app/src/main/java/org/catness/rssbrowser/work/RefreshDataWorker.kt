package org.catness.rssbrowser.work

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import org.catness.rssbrowser.database.ArticleDatabase.Companion.getDatabase
import org.catness.rssbrowser.repository.ArticleRepository
import retrofit2.HttpException


class RefreshDataWorker(appContext: Context, params: WorkerParameters) :
        CoroutineWorker(appContext, params) {

    companion object {
        const val WORK_NAME = "org.catness.rssbrowser.work.RefreshDataWorker"
    }
    override suspend fun doWork(): Result {
        val database = getDatabase(applicationContext)
        val repository = ArticleRepository(database)

        try {
            repository.refreshArticles()
            Log.i("RefreshDataWorker","WorkManager: Work request for sync is run")
        } catch (e: HttpException) {
            return Result.retry()
        }
        return Result.success()
    }
}