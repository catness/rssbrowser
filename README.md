# About the project

**RSSBrowser** is an example for [Android Kotlin Fundamentals: repository (offline cache) and WorkManager](
https://codelabs.developers.google.com/codelabs/kotlin-android-training-repository/). Instead of the DevByte feed, it uses the Slashdot RSS feed, and shows the article description, with the link that can be opened in a browser. So I also had to switch from JSON to XML parsing, which is possible with tickaroo, thanks to the detailed explanation from [Android-Kotlin XML Parsing with Retrofit](https://medium.com/@sameerzinzuwadia/android-kotlin-xml-parsing-with-retrofit-6879401d7901).

To practice conversion between domain, database and network models, I used the date field - it comes as ISO-formatted text in XML, stored as Unix timestamp in the database, and displayed as text in a different format in the app.

The RSS feed is refreshed twice per hour by WorkManager. However the app seems to save the position of the article which was the newest when the user opened the app previously - it remains on top of the screen, so the user has to scroll up to see the newer articles. I decided not to bother with it, because it's even sort of useful (you can see which articles are new).

Articles older than 30 days are automatically deleted.

The apk is here: [rssbrowser.apk](apk/rssbrowser.apk).
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)



